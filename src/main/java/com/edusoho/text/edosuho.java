package com.edusoho.text;

import com.edusoho.entity.Deiver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class edosuho {
    public static void main(String[] args) throws InterruptedException {
        Deiver liulan=new Deiver("webdriver.chrome.driver","C:/driver/chromedriver.exe");
        System.setProperty(liulan.getBrowser(),liulan.getdate());
        WebDriver driver=new ChromeDriver();
        driver.get("http://qa.edusoho.cn/");
        //  WebDriverWait类是由WebDirver提供的等待方法。在设置时间内，默认每隔一段时间检测
        //  一次当前页面元素是否存在，如果超过设置时间检测不到则抛出异常
        WebDriverWait wait=new WebDriverWait(driver,10,1);
        /////创建鼠标属性方法,--------------然后获取id为kw的元素并点击:action.moveToElement(driver.findElement(By.id("kw"))).perform();。
        Actions actions=new Actions(driver);
        //name
        actions.moveToElement(driver.findElement(By.id("login_username"))).perform();
        driver.findElement(By.id("login_username")).sendKeys("李琦");
        //password
        actions.moveToElement(driver.findElement(By.id("login_password"))).perform();
        driver.findElement(By.id("login_password")).sendKeys("123456");
        //通过元素类型找到按钮，并对按钮进行点击操作；
        driver.findElement(By.xpath("//button")).click();
       // driver.findElement(By.xpath(""));
        //通过元素文本找到按钮，并对按钮进行点击操作；
        driver.findElement(By.xpath("//a[contains(text(),'管理')]")).click();
        //Thread.Sleep()方法用于将当前线程休眠一定时间
        // 时间单位是毫秒 1000毫秒= 1秒 休眠的时间可以用于让其他线程完成当前工作
        // ，亦可以减少CPU占用时间。避免程序出现长时间CPU占用100%的情况。
        Thread.sleep(2000);
        //通过元素文本，并对进行点击操作；
        driver.findElement(By.xpath("//a[contains(text(),'培训')]")).click();
        Thread.sleep(1000);
        //通过元素属性值，并对进行点击操作；
        driver.findElement(By.xpath("//a[@title='培训项目']")).click();
        Thread.sleep(500);

        driver.findElement(By.xpath("//a[@class='btn btn-success']")).click();
        //通过元素id找到其位置，并对文本框进行输入值；
        driver.findElement(By.id("name")).sendKeys("自动的培训项目");
         //  /admin/setting/developer


        System.out.print("成功");






    }
}
