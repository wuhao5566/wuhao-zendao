package com.edusoho.util;

import com.edusoho.testcase.BaseTestCase;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FunctionUtil {

    //Set global parameters for all test2 cases

    //Set global parameters for all test2 cases
    static int MAX_WAIT_FOR_PAGE = Integer.parseInt(SystemUtil.resource.getProperty("maxWaitForPage.Time"));
    static int MAX_WAIT_FOR_ELEMENT = Integer.parseInt(SystemUtil.resource.getProperty("maxWaitForElement.Time"));
    static int SLEEP_IN_MILLIONS = Integer.parseInt(SystemUtil.resource.getProperty("sleepInMillions.Time"));
    static WebDriverWait wait = new WebDriverWait(BaseTestCase.threadDriver.get(),MAX_WAIT_FOR_ELEMENT,SLEEP_IN_MILLIONS);

    //Instance Pages
    static String screenMonitorPath = SystemUtil.resource.getProperty("screenMonitor.path");
    public static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>();




    /**
     * Objective: click element by JavascriptExecutor
     */
    static public void clickByJs(WebElement element) throws Exception {

        try {
            WebDriver driver = BaseTestCase.threadDriver.get();
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();",element);
        } catch (Exception ex) {
            System.out.println( element +"Element is not clickable");
            ex.getMessage();
        }

    }


}
