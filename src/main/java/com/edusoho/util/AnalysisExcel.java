package com.edusoho.util;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.*;

public class AnalysisExcel {
    Workbook workbook = null;
    //C:\Users\kuozhi\Desktop
    File Inputfile = new File("C:\\Users\\kuozhi\\Desktop\\课程购买支付 (1).xls");
    File outputfile =new File("C:\\Users\\kuozhi\\Desktop\\GPSINFO.txt");

    public void ToAnalysisExcel() {
        // Unable to recognize OLE stream  不支持xlsx格式  支持xls格式

        try {

            FileInputStream fileInputStream = new FileInputStream(Inputfile);
            workbook = Workbook.getWorkbook(fileInputStream);
            FileOutputStream fileOutputStream = new FileOutputStream(outputfile);
            BufferedOutputStream bw = new BufferedOutputStream(fileOutputStream);   //输出语句

            Sheet readfirst = workbook.getSheet(0);
            int rows = readfirst.getRows();
            int clomns = readfirst.getColumns();
            System.out.println("row:" + rows);
            System.out.println("clomns:" + clomns);

            for(int i =1;i<rows;i++) {
                Cell[] cells = readfirst.getRow(i); //循环得到每一行的单元格对象

                //根据每一个单元格对象的到里面的值
                String brandNum= cells[3].getContents();
                String deviceCode = cells[4].getContents();
                String sim =cells[5].getContents();
                String six =cells[6].getContents();
                System.out.println("rand:"+brandNum+",vehicleNum:"+deviceCode+",sim:"+sim);

                //将得到的值放在一个我需要的格式的string对象中

                String  output = "\n"+"开头"+"\n" +
                        "{\n" +
                        "  \"brandColor\": 500000,\n" +
                        "  \"用例名称/测试点\": \""+brandNum+"\",\n" +
                        "  \"预置条件/前提条件\": \""+deviceCode+"\",\n" +
                        "  \"测试步骤\": \""+sim+"\"\n" +
                        "  \"预期结果\": \""+six+"\"\n" +
                        "}"+
                        "\n";
                //write and flush到 我需要的文件中去，flush后才能成功
                byte[] outputbyte = new String(output).getBytes();
                bw.write(outputbyte);

                bw.flush();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }

    }

}
