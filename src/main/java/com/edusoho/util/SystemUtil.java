package com.edusoho.util;

import java.io.InputStream;
import java.util.Properties;

public class SystemUtil {

    static public Properties resource = null;

    /**
     * load the test data in properties file
     * @param filePath : The relative path of the properties file
     * 
     */
    static {
        InputStream dataInput = SystemUtil.class.getResourceAsStream("/testdata_common.properties");
        resource = new Properties();
        try {
            resource.load(dataInput);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
