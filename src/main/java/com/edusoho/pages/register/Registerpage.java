package com.edusoho.pages.register;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Registerpage {

    //注册邮箱或手机号
    @FindBy(xpath = "//input[@id='register_emailOrMobile']")
    public WebElement input_email;

    //注册手机

    //注册邮箱

    //注册用户名
    @FindBy(xpath = "//input[@id='register_nickname']")
    public WebElement input_Nickname;

    //注册密码
    @FindBy(xpath = "//input[@id='register_password']")
    public WebElement input_Nickpassword;

    //邮箱验证码
    @FindBy(xpath = "//div[@id='drag-btn']")
    public WebElement btn_drg;

    //手机验证码

    //发送短信

    //立即注册
    @FindBy(xpath = "//a[contains(text(),'立即注册')]")
    public WebElement a_Immediate_registration;

}
