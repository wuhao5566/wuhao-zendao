package com.edusoho.pages.login;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Loginpage {

    //登录用户名
    @FindBy(xpath = "//input[@id='login_username']")
    public WebElement input_Username;

    //登录密码
    @FindBy(xpath = "//input[@id='login_password']")
    public WebElement input_Password;

    //登录按钮
    @FindBy(xpath = "(//button[@type='button'])[2]")
    public WebElement btn_Login;

    //立即注册
    @FindBy(xpath = "//a[contains(text(),'立即注册')]")
    public WebElement a_Register;

}
