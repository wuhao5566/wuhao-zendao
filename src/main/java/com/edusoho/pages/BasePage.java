package com.edusoho.pages;

import com.edusoho.testcase.BaseTestCase;
import com.edusoho.util.FunctionUtil;
import com.edusoho.util.SystemUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BasePage extends BaseTestCase {
    //重复页面元素，比如导航栏

        //homePagrurl啥意思
        private static String homePageUrl= SystemUtil.resource.getProperty("homePagr.url");

        //导航栏logo
        @FindBy(xpath = "(//a[contains(@href, '/')])[21]")
        public WebElement logo;
        //首页
        @FindBy(xpath = "//ul[@id='nav']/li/a")
        public WebElement homepage;
        //班级
        @FindBy(xpath = "//ul[@id='nav']/li[2]/a")
        public  WebElement clases;
        //咨询
        @FindBy(xpath = "//ul[@id='nav']/li[3]/a")
        public WebElement Consultation;
        //会员
        @FindBy(xpath = "//ul[@id='nav']/li[6]/ul/li/a")
        public WebElement vip;
        //修改导航和logo，气球与=鱼学堂logo
        @FindBy(xpath = "//ul[@id='nav']/li[6]/ul/li[2]/a")
        public WebElement Modify_Documents;
        //小组
        @FindBy(xpath = "//ul[@id='nav']/li[6]/ul/li[3]/a")
        public WebElement group;
        //账户中心
        @FindBy(xpath = "//a[contains(@href, '/my/orders')]")
        public WebElement Account_Center;
        //个人中心
        @FindBy(xpath = "//a[contains(@href, '/settings')]")
        public WebElement Personal_Center;

        //目标导航到登录页面
        public void gotoLoginInPage(){

        BaseTestCase.threadDriver.get().navigate().to(homePageUrl);

        }

        //注销
        public void logOut(){
                //FunctionUtil.clickByJs();

        }
}
