package com.edusoho.testcase;
import java.io.File;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;

import com.beust.jcommander.Parameter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeMethod;

import java.io.File;

public class BaseTestCase {
    //驱动
    public WebDriver drivei;
    //ThreadLocal的是一个本地线程副本变量工具类
    public static ThreadLocal<WebDriver> threadDriver=new ThreadLocal<WebDriver>();
    //浏览器标识
    public static String browserFlag;

    //备份数据库


    //打开浏览器
    @Parameter(names = {"browserflag0"})
    @BeforeMethod(alwaysRun = true)
    public void setUpBrowers(String browserFlag0){
        browserFlag=browserFlag0;
        //先判断客户端的系统 在进行不同的操作
        String getSystemName=System.getProperty("os.name").toLowerCase();
        //如果是win端
        if(getSystemName.contains("winodws")){
            File firefoxFile = new File("./lib/geckodriver.exe");
            File chromeFile = new File("./lib/chromedriver.exe");
            File ieFile = new File("./lib/iedriverserver.exe");
            System.setProperty("webdriver.gecko.driver", firefoxFile.getAbsolutePath());
            System.setProperty("webdriver.chrome.driver", chromeFile.getAbsolutePath());
            System.setProperty("webdriver.ie.driver", ieFile.getAbsolutePath());

            //mac端
        }else if (getSystemName.contains("mac")){



            //如果还不是就是Linux端
        }else{

        }

        ///不懂
        if (browserFlag0.equals("0")){
                drivei =new FirefoxDriver();
        }else if(browserFlag0.equals("1")){
                drivei =new ChromeDriver();
        }else if(browserFlag0.equals("2")){
                drivei =new InternetExplorerDriver();
        }
        //意思？
        threadDriver.set(drivei);
        ///封装到了util里//意思？
        BaseTestCase.threadDriver.get().manage().window();
    }
    ///关闭由Selenium服务器打开的已打开浏览器
    @AfterMethod(alwaysRun = true)
    public void tearDown(){
    drivei.close();
    }
}
